// Full list of configuration options available at:
// https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({
	controls: true,
	progress: true,
	history: true,
	center: true,
	mouseWheel: true,

	transition: 'slide', // none/fade/slide/convex/concave/zoom

	// Optional reveal.js plugins
	dependencies: [
		{ src: 'reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
		{ src: 'reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
		{ src: 'reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
		{ src: 'reveal.js/plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
		{ src: 'reveal.js/plugin/zoom-js/zoom.js', async: true },
		{ src: 'reveal.js/plugin/notes/notes.js', async: true }
	]
});

Reveal.addEventListener ('ready', moveTitle);
Reveal.addEventListener ('ready', moveSlide);
Reveal.addEventListener ('ready', tweakStyles);

Reveal.addEventListener ('slidechanged', moveTitle);
Reveal.addEventListener ('slidechanged', moveSlide);

$(window).on ('resize', tweakStyles);

// Move title to top of page
function moveTitle () {
	$('div.main-title-container').empty ();
	if (!$('section.present').is (':first-child')) {
		var $node = $('div.slides>section.present h1').clone ()
			.css ({
				'font-size': $('div.slides>section.present>section.present h2').css ('font-size')
			});
		$('div.main-title-container').empty ().append ($node);
	}
}

function moveSlide () {
	if (Reveal.getScale () < 0.5) {
		console.log ('moveSlide');
		$('div.slides').css ('transform', 'translate(-50%,-' + (50 + (50 * Reveal.getScale ())) + '%) scale(' + Reveal.getScale () + ')');
	}
}

function tweakStyles () {
	$('div.main-title-container').css ('transform', 'scale(1,' + Reveal.getScale () + ')');
	if (Reveal.getScale () > 1) {
		$('figure').css ('transform', 'scale('+ (1/Reveal.getScale ()) + ')');
	}
}
